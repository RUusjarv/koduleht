<?php 
require_once("functions.php");
$myurl=$_SERVER['PHP_SELF'];
alusta_sessioon();
connect_db();
 
$mode="pealeht";				
			
if (isset($_GET["mode"]) && $_GET["mode"]!=""){
	$mode=$_GET["mode"];
	}				

include_once("view/head.php");

switch($mode){	
case "logIn":
	kuva_login();
break;
case "logOut":
	logout();
break;
case "registreeru":
	kuva_registreeru();
break;
case "register":
	registreeri();
break;
case "lisakommentaar":
	autoriseeri();
	komment();
break;
case "kustuta":
	autoriseeri();
	kustuta();
break;
case "pealeht":
	kuva_pealeht();
break;		
case "galerii":
	kuva_galerii();
break;
case "minu_galerii":
	autoriseeri();
	kuva_kasutaja_galerii();
break;	
case "edit":
	autoriseeri();
	kuva_vorm('update');
break;
case "add":
	autoriseeri();
	kuva_vorm('insert');
break;
case "insert":
	autoriseeri();
	teosta_insert();
break;
case "update":
	autoriseeri();
	teosta_update();
break;
case "pilt":
	kuva_pilt();
break;			
	default:
include("view/pealeht.php");
}
include_once("view/foot.php");
 

?>