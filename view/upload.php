<div id="tabelid">
<p> Maksimaalne üleslaadimise suurus on 100kb	</p>
<form action="<?php echo $myurl.'?mode='.$action; ?>" method="POST" enctype="multipart/form-data">

	<table>
		<tr><td colspan="2"><?php if($action=="update"): ?>
		<a href="<?php echo htmlspecialchars($pilt['pilt']);?>" title="Vaata suuremalt">
			<img src="<?php echo htmlspecialchars($pilt['thumb']);?>" />
		</a>
<?php endif; ?>	</td></tr>
		
		<?php if($action=="update"): ?>
			<tr><td><input type="hidden" name="id" value="<?php echo htmlspecialchars($pilt['id']);?>" /></td></tr>
		<?php endif; ?>
		<tr><td>Pealkiri</td><td> <input type="text" name="pealkiri" value="<?php if (isset($pilt['pealkiri'])) echo htmlspecialchars($pilt['pealkiri']); ?>"/></td></tr>
		<?php if($action=="update" && $_SESSION['role']=="admin"): ?>
			<tr><td>Autor</td><td>
				<select name="autor">
					<option>Vali autor!</option>
					<?php foreach($users as $user):?>
						<option  value="<?php echo htmlspecialchars($user['id']);?>" <?php if($pilt['kasutaja_id']==$user['id']) echo 'selected="selected"';?> ><?php echo htmlspecialchars($user['Kasutaja']);?></option>
					<?php endforeach; ?>
				</select>
			</td></tr>
		<?php else:?>
			<input type="hidden" name="autor" value="<?php echo htmlspecialchars($_SESSION['user_id']);?>"/>	
		<?php endif; ?>
		<tr><td>Kirjeldus: </td><td></td></tr>
		<tr><td colspan="2"><textarea name="Kirjeldus" rows="10" cols="50"><?php if (isset($pilt['Kirjeldus'])) echo htmlspecialchars($pilt['Kirjeldus']); ?></textarea></td></tr>
		<tr><td>Väike pilt</td><td><input type="file" name="small"></td></tr>
		<tr><td>Suur pilt</td><td><input type="file" name="big"></td></tr>
		<tr><td><input type="submit" name="button" value="<?php if($action=="update") echo "Muuda"; else echo "Lisa";?>"/></td><td><input type="submit" name="button" value="Katkesta"/></td></tr>
	</table>
</form>
</div>

<?php if (isset($veateated)):?>
	<?php foreach($veateated as $veateade):?>
		<p style="color:red">
	<?php echo htmlspecialchars($veateade); ?>
		<?php endforeach;?>
		</p>
<?php endif;?>

</form>
