<h2>Meie väärtused</h2>

<h3>Safety</h3>
<p>
At Ehitus OÜ, we believe that safety, quality, and production are equally important to growing our business. We work daily to create a world-class work environment where all employees attend to safety issues through education in hazard recognition and avoidance, with management leading by example to demonstrate this all-inclusive commitment to safety.
</p>
<h3>People</h3>
<p>
We challenge ourselves to excel and believe that everyone at Sterling has opportunities for career growth. We identify, hire, and train the most capable staff and reward superior performance. We believe that candor among all employees is essential to providing sincere evaluations of job execution. We recognize and appreciate our responsibility to the community and are committed to improving the health, safety, and welfare of the people in the communities we serve.
</p>
<h3>Quality</h3>
<p>
We take pride in providing a quality product on time, the first time. We endeavor to place a Sterling signature on all our projects by consistently meeting the requirements of the specifications and by delivering sustainable projects that are a superior value to the owner.
</p>
<h3>Innovation</h3>
<p>
We strive to develop innovative methods to construct projects more efficiently and take challenging problems and turn them into opportunities for creative solutions.
</p>