<!DOCTYPE html>
  <html lang="et">
  <head>
    <meta charset="utf-8"/>
	<script type="text/javascript" src="pildi.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="stiil.css" />
	<link rel="stylesheet" type="text/css" href="pildi.css" />
    <title>OÜ Ehitus</title>
  </head>
  <body onload="startTime()">
  <div id="keha">

  <ul id ="menu">
			<li><a href="?mode=pealeht">VÄÄRTUSED</a></li>
			<li><a href="?mode=galerii">TEHTUD TÖÖD</a></li>
			<?php if(isset($_SESSION['username'])):?>	
			<li><a href="?mode=add">LISA PILT </a></li>
			<li><a href="?mode=minu_galerii">MINU PILDID</a></li>
			<li><a href="?mode=logOut">LOGI VÄLJA</a></li>
			<?php else: ?>
			<li><a href="?mode=logIn">LOGI SISSE</a></li>
			<li><a href="?mode=registreeru">REGISTREERU </a></li>
			<?php endif; ?>
</ul>
<div id="sisu">
	<div id="tervitus">
	<?php if(isset($_SESSION['teade'])): ?>
  			<?php foreach($_SESSION['teade'] as $note):?>
    			<?php echo $note; ?>
  			<?php endforeach;
    		unset($_SESSION['teade']); ?> 
	<?php endif;?>
	</div>
	