window.onload = function() {
	pildid=document.getElementById('galerii');
	if (galerii != null) {	
		lingid=pildid.getElementsByTagName("href");
		for (i=0; i<lingid.length; i++){
			lingid[i].onclick=function(){
				showDetails(this);
				return false;
			}
		}
	}
}
function showDetails(el){
	hoidja=document.getElementById('hoidja');
	if (hoidja != null) {
		$.get(el.href, "html", function(data){
	document.getElementById('sisu').innerHTML=data;
	});
		hoidja.style.display="initial";
	}

}
function hideDetails() {
	document.getElementById('hoidja').style.display="none";
}
function suurus(el){
  el.removeAttribute("height"); // eemaldab suuruse
  el.removeAttribute("width");
  if (el.width>window.innerWidth || el.height>window.innerHeight){  // ainult liiga suure pildi korral
    if (window.innerWidth >= window.innerHeight){ // lai aken
      el.height=window.innerHeight*0.5; // 90% k�rgune
      if (el.width>window.innerWidth){ // kas element l�heb ikka �le piiri?
        el.removeAttribute("height");
        el.width=window.innerWidth*0.9;
      }
    } else { // kitsas aken
      el.width=window.innerWidth*0.5;   // 90% laiune
      if (el.height>window.innerHeight){ // kas element l�heb ikka �le piiri?
        el.removeAttribute("width");
        el.height=window.innerHeight*0.9;
      }
    }
  }
}
window.onresize=function() {
	suurpilt=document.getElementById("suurpilt");
	suurus(suurpilt);
}
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i}; 
    return i;
}