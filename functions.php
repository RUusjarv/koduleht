﻿<?php

function connect_db(){
  global $connection;
  $host="localhost";
  $user="test";
  $pass="t3st3r123";
  $db="test";
  $connection = mysqli_connect($host, $user, $pass, $db) or die("Ei saa mootoriga ühendust");
  mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}
function alusta_sessioon(){
	// siin ees võiks muuta ka sessiooni kehtivusaega, aga see pole hetkel tähtis
	session_start();
	}
	
function lopeta_sessioon(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
}

function autoriseeri(){
	global $myurl;
	if (isset($_SESSION['username']) && $_SESSION['username']!=""){
		// sisselogitud
		return true;
	} else {
		// ei ole sisse logitud, saada teatega esilehele.
		$_SESSION['teade'][]="Selle lehe kuvamiseks pead olema sisse logitud";
		header("Location: $myurl?mode=login");	
		exit(0);
	}
}

function hangi_pildid(){
	global $connection;
	$pildid=array();
	$query ="SELECT p.id, p.thumb, p.pilt, p.pealkiri, k.Kasutaja as kasutaja_id, p.Kirjeldus FROM 10132492_pildid p, 10132492_kasutajad k where k.id=p.kasutaja_id ORDER BY p.ID ASC";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$row['thumb']=htmlspecialchars('thumb/'.$row['thumb']);
		$row['pilt']=htmlspecialchars('img/'.$row['pilt']);
		$row['alt']=htmlspecialchars($row['pealkiri']);
		$row['autor']=htmlspecialchars($row['kasutaja_id']);
		$row['kirjeldus']=htmlspecialchars($row['Kirjeldus']);
		$pildid[$row['id']]=$row;
	}
	return $pildid;
}

function hangi_kasutaja_pildid($id){
		global $connection;
	$pildid=array();
	$id=mysqli_real_escape_string($connection, $id);
	$query ="SELECT p.id, p.thumb, k.Kasutaja as kasutaja_id, p.pealkiri FROM 10132492_pildid p, 10132492_kasutajad k where k.id=p.kasutaja_id and k.id=$id";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		// tekstilised väljad on vaja saniteerida
		$row['alt']=htmlspecialchars($row['pealkiri']);
		$row['autor']=htmlspecialchars($row['kasutaja_id']);
		$row['thumb']=htmlspecialchars('thumb/'.$row['thumb']);
		$pildid[$row['id']]=$row;
	}
	return $pildid;
	}


function hangi_pilt($id){
	global $connection;
	$pilt=array();
	$id=mysqli_real_escape_string($connection, $id);
	if (!is_numeric($id)) return false;
	$query ="SELECT p.id, p.thumb, p.pilt, p.pealkiri, k.Kasutaja as kasutaja_id, p.Kirjeldus FROM 10132492_pildid p, 10132492_kasutajad k where p.id=$id and k.id=p.kasutaja_id";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		
		$row['thumb']=htmlspecialchars('thumb/'.$row['thumb']);
		$row['pilt']=htmlspecialchars('img/'.$row['pilt']);
		$row['alt']=htmlspecialchars($row['pealkiri']);
		$row['autor']=htmlspecialchars($row['kasutaja_id']);
		$row['kirjeldus']=htmlspecialchars($row['Kirjeldus']);
		$pilt=$row;
	}
	return $pilt;
}

function pildi_info($id){
	global $connection;
	$pilt=array();
	$id=mysqli_real_escape_string($connection, $id);
	if (!is_numeric($id)) return false;
	$query ="SELECT * FROM 10132492_pildid WHERE id=$id";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){

		$row['alt']=htmlspecialchars($row['pealkiri']);
		$row['pilt']=htmlspecialchars('img/'.$row['pilt']);
		$row['thumb']=htmlspecialchars('thumb/'.$row['thumb']);
		$pilt=$row;
	}
	return $pilt;
}

function uuenda_pilt($id, $pilt, $field  ){
	global $connection;
	
	$id=mysqli_real_escape_string($connection, $id);
	$pilt=mysqli_real_escape_string($connection, $pilt);
	$field=mysqli_real_escape_string($connection, $field);	

	$query ="update 10132492_pildid set $field='$pilt' WHERE id=$id";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	return mysqli_affected_rows($connection);	
}
function uuenda_pildi_info($id, $pealkiri, $kasutaja_id, $desc){
	global $connection;

	$id=mysqli_real_escape_string($connection, $id);
	$pealkiri=mysqli_real_escape_string($connection, $pealkiri);
	$kasutaja_id=mysqli_real_escape_string($connection, $kasutaja_id);
	$desc=mysqli_real_escape_string($connection, $desc);

	$query ="update 10132492_pildid set kasutaja_id='$kasutaja_id', pealkiri='$pealkiri', Kirjeldus='$desc' WHERE id=$id";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	return mysqli_affected_rows($connection);	
}

function lisa_pilt($pilt){
	global $connection;

	foreach($pilt as &$field){
		$field=mysqli_real_escape_string($connection, $field);
	}
	$query ="insert into 10132492_pildid (thumb, pilt, kasutaja_id, pealkiri, Kirjeldus) values ('{$pilt['small']}', '{$pilt['big']}', '{$pilt['autor']}', '{$pilt['pealkiri']}','{$pilt['Kirjeldus']}')";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	return mysqli_insert_id($connection);	
	}

function upload($name, $loc){
  $allowedExts = array("jpg", "jpeg", "gif", "png");
  $allowedTypes = array("image/gif", "image/jpeg", "image/png","image/pjpeg");
  $test=explode(".", $_FILES[$name]["name"]);
  $extension = end($test);

  if ( in_array($_FILES[$name]["type"], $allowedTypes)
   && ($_FILES[$name]["size"] < 100000) // see on 100kb
   && in_array($extension, $allowedExts)) {
    // fail õiget tüüpi ja suurusega
    if ($_FILES[$name]["error"] > 0) {
      return "";
    } else {
      // vigu ei ole
      if (file_exists($loc."/" . $_FILES[$name]["name"])) {
        // fail olemas ära uuesti lae, tagasta failinimi
        return $_FILES[$name]["name"];
      } else {
        // kõik ok, aseta pilt
        move_uploaded_file($_FILES[$name]["tmp_name"], $loc."/" . $_FILES[$name]["name"]);
        return $_FILES[$name]["name"];
      }
    }
  } else {
    return "";
  }
}

function kontrolli_saadavus($username){ 
	global $connection;

	$username=mysqli_real_escape_string($connection, $username);

	$query ="SELECT * FROM 10132492_kasutajad WHERE Kasutaja='$username'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	if (mysqli_num_rows($result)>0) {
		return true;
	}else { 
		return false;
	}
}

function lisa_kasutaja($username, $password) {
	global $connection;
	$result=kontrolli_saadavus($username); 
	
	$username=mysqli_real_escape_string($connection, $username);
	$password=mysqli_real_escape_string($connection, $password);

	$query ="insert into 10132492_kasutajad (Kasutaja, Parool) values ('$username', sha1('$password'))";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
	return mysqli_insert_id($connection);	
}

function kontrolli_kasutaja($username, $password) {
	
	global $connection;
	$username=mysqli_real_escape_string($connection, $username);
	$password=mysqli_real_escape_string($connection, $password);

	$query ="SELECT id, Kasutaja, Parool, role FROM 10132492_kasutajad WHERE Kasutaja='$username' and Parool=sha1('$password')";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	$user=mysqli_fetch_assoc($result);
	if (!empty($user)) {
		return $user;	
	}	else {
		return false;	
	}
}

function koik_kasutajad(){
	global $connection;
	$users=array();
	$query ="SELECT id, Kasutaja FROM 10132492_kasutajad";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$users[]=$row;
	}
	return $users;
}

function kuva_login(){
	
	if (isset($_POST["login"] )){
	$username="";
	$passwd="";
	$veateated=array();
	if (isset($_POST["username"]) && $_POST["username"]!="") {
		$username=$_POST["username"];
	} else {
		$veateated[]="Kasutajanimi puudu";
	}
	if (isset($_POST["passwd"]) && $_POST["passwd"]!="") {
		$passwd=$_POST['passwd'];
	} else {
		$veateated[]="Parool puudu";	
	}
	if ($user=kontrolli_kasutaja($username, $passwd)){ 
			$_SESSION['username']=$username;
			$_SESSION['user_id']=$user['id']; 
			$_SESSION['role']=$user['role'];
			$_SESSION['teade'][]="Tere $username, sisselogimine õnnestus!";
			header('Location: kontroller.php');
		} 	
	if  (isset($_POST["username"]) && $_POST["username"]!="" && isset($_POST["passwd"]) && $_POST["passwd"]!="") {
			$veateated[]="Sisetasid vale kasutajanime ja parooli koosluse";	
		}		
		include("view/logIn.php");
	}
	else {
		include("view/logIn.php");
	}
}

function kuva_registreeru(){
	include_once("view/register.php");
}

function kuva_vorm($action){
	global $myurl;
	if ($action=="update"){ // TEGELETAKSE PILDI INFO MUUTMISEGGA
			if (isset($_GET['id']) && is_numeric($_GET['id'])) {
			$id=$_GET['id'];
			$users=koik_kasutajad();
			$pildid=koik_pildid_id();
			// leiame kas sellise pildi id on piltide massiivis üldse olemas (jätame võtme muutujasse $p meelde)
			$p=array_search($id, $pildid);
			if ($p===FALSE) {
				$_SESSION['teade'][]="Sellist pilti ei ole."; 
				header("Location: $myurl"); 	// pilti polnud, suuna pealehele
			}
			// pilt olemas, hangi info ja kuva vorm
			$pilt=pildi_info($id);
			
			// kontrollime, kas sellel kasutajal on üldse õigusi selle pildi muutmiseks
			if ($pilt['kasutaja_id']!=$_SESSION['user_id'] && $_SESSION['role']!="admin" ){
				$_SESSION['teade'][]="Sul puuduvad õigused selle pildi muutmiseks.";
				header("Location: $myurl");	
			}
			
			include_once('view/upload.php');
		} else {
			$_SESSION['teade'][]="Sellist lehte ei eksisteeri.";
			header("Location: $myurl");
		}
	} else {
		include_once('view/upload.php');
	}
}	
	
function teosta_update(){
	global $myurl;
	$action="update";
	$veateated=array();
	$pilt=array();
	
	// kõigepealt kontrolli kas taheti salvestada
	if (isset($_POST['button']) && $_POST['button']!="") {
		$nupp=$_POST['button'];
		if ($nupp=="katkesta") {
			$_SESSION['teade'][]="Pildi muutmine tühistatud.";
			header("Location: $myurl");	
			exit(0);
		}
	}
	
	// valideerimine ja puhastamine
	if (isset($_POST['id']) && $_POST['id']!="") {
		$pilt['id']=$_POST['id'];
	} else {
		$veateated[]="Katkine vorm";	
	}
	
	$pilt=pildi_info($pilt['id']);
	
	// kontrollime, kas sellel kasutajal on üldse õigusi selle pildi muutmiseks
	if ($pilt['kasutaja_id']!=$_SESSION['user_id'] && $_SESSION['role']!="admin" ){
		$_SESSION['teade'][]="Sul puuduvad õigused selle pildi muutmiseks.";
		header("Location: $myurl");	
	}
	
	if (isset($_POST['pealkiri']) && $_POST['pealkiri']!="") {
		$pilt['pealkiri']=$_POST['pealkiri'];
	} else {
		$veateated[]="Pealkiri puudu";	
	}
	if (isset($_POST['autor']) && $_POST['autor']!="") {
		$pilt['kasutaja_id']=$_POST['autor'];
	} else {
		$veateated[]="Autor puudu";	
	}
	if (isset($_POST['Kirjeldus']) && $_POST['Kirjeldus']!="") {
		$pilt['Kirjeldus']=$_POST['Kirjeldus'];
	} else {
		$veateated[]="Kirjeldus puudu";	
	}
	// info baasi lükkamine kui vigu pole
	if (empty($veateated)) {
		$result=uuenda_pildi_info($pilt['id'], $pilt['pealkiri'], $pilt['kasutaja_id'], $pilt['Kirjeldus'] );
		
		// ürita faile laadida
		$small=upload("small", "thumb");
		if (trim($small)!="" ){
			uuenda_pilt($pilt['id'], $small, "thumb");
		}
		$big=upload("big", "img");
		if (trim($big)!=""){
			uuenda_pilt($pilt['id'], $big, "pilt");
		} 
		$_SESSION['teade'][]="Pildi info muudetud";
		header("Location: $myurl?mode=pilt&id={$pilt['id']}");
	}else {
		include("view/upload.php");	
	}	
}

function teosta_insert(){
	global $myurl;
	$action="insert";
	$veateated=array();
	$pilt=array();
	
	// kõigepealt kontrolli kas taheti salvestada
	if (isset($_POST['button']) && $_POST['button']!="") {
		$nupp=$_POST['button'];
		if ($nupp=="katkesta") {
			$_SESSION['teade'][]="Pildi lisamine tühistatud.";
			header("Location: $myurl");	
			exit(0);
		}
	}
	// valideerimine ja puhastamine
	if (isset($_POST['pealkiri']) && $_POST['pealkiri']!="") {
		$pilt['pealkiri']=$_POST['pealkiri'];
	} else {
		$veateated[]="Pealkiri puudu";	
	}
		$pilt['autor']=$_SESSION['user_id'];
	if (isset($_POST['Kirjeldus']) && $_POST['Kirjeldus']!="") {
		$pilt['Kirjeldus']=$_POST['Kirjeldus'];
	} else {
		$veateated[]="Kirjeldus puudu";	
	}
	// info baasi lükkamine kui vigu pole
	if (empty($veateated)) {
		// ürita faile laadida
		$pilt['small']=upload("small", "thumb");
		$pilt['big']=upload("big", "img");
		// kas õnnestus?
		if (trim($pilt['big'])!="" && trim($pilt['small'])!=""){
			$pildi_id=lisa_pilt($pilt);
			if ($pildi_id) {
				$_SESSION['teade'][]="Pilt lisatud";
				header("Location: $myurl?mode=pilt&id={$pildi_id}");
				exit(0);
			} else {
				$veateated[]="Viga pildi info lisamisel";
			}
		} else {
				$veateated[]="Viga pildi üles laadimisel";
		}
		include("view/upload.php");
	}else {
		include("view/upload.php");	
	}	
}


function registreeri() {
	global $connection;
	$veateated=array();
	if (isset($_POST['username']) && $_POST['username']!=""){
		$username=$_POST['username'];
	}	else {
		$veateated[]="Kasutajanimi puudu";	
	}	
	if (isset($_POST['passwd1']) && $_POST['passwd1']!=""){
		$p1=$_POST['passwd1'];
	}	else {
		$veateated[]="Esimene parooliväli tühi";	
	}
	if (isset($_POST['passwd2']) && $_POST['passwd2']!=""){
		$p2=$_POST['passwd2'];
	}	else {
		$veateated[]="Teine parooliväli tühi";	
	}
	if (isset($p1) && isset($p2)){
		if ($p1==$p2) {
			$password=$p1;	
		} else {
			$veateated[]="Parooliväljade sisu on erinev";	
		}
	}	
	
    if(empty($veateated) && $result=kontrolli_saadavus($username)){
		$veateated[]="Selline kasutaja on juba olemas";	
	}
	if (empty($veateated)){
		$result=lisa_kasutaja($username, $password);
		if ($result){
			$_SESSION['teade'][]="Kasutaja $username registreerumine õnnestus, võid nüüd sisse logida";
			header('Location: kontroller.php?mode=logIn');
			exit(0);
		} else { 
			include("view/register.php");
		} 
	} else {
		include("view/register.php");
	}
}

function kuva_galerii(){
	global $myurl, $pildid;
	$pildid=hangi_pildid();
	$_SESSION['last_gallery']=$myurl;
	include("view/galerii.php");
}

function kuva_kasutaja_galerii(){
	global $myurl, $pildid;
	if (isset($_SESSION['user_id'])){
		if($_SESSION['role']==="admin"){
		$_SESSION['last_gallery']=$myurl."?mode=my_gallery";
		$pildid=hangi_pildid();
		include("view/minu.php");
		}else{
		$_SESSION['last_gallery']=$myurl."?mode=my_gallery";
		$pildid=hangi_kasutaja_pildid($_SESSION['user_id']);
		include("view/minu.php");
		}
	} else {
		$_SESSION['teade'][]="Ei suutnud kasutajat tuvastada";
		header("Location: $myurl");	
	}
}

function logout(){
		lopeta_sessioon();
		header('Location: kontroller.php?mode=logIn');
	}


function kuva_pealeht (){
	include ("view/pealeht.php");
}

function kuva_pilt(){
global $myurl;
	$pildid=koik_pildid_id();
	$kommentaarid=koik_kommentaarid_id();
	$eelmine=-1;
	$jargmine=-1;
	$kommentaar=array();
	if (isset($_GET['id']) && is_numeric($_GET['id'])) {
		$id=$_GET['id'];
		$p=array_search($id, $pildid);
		if ($p===FALSE) header("Location: $myurl");
		$pilt=hangi_pilt($id);
		$kommentaar=hangi_kommentaarid($id);
		if ($p>0) $eelmine=$pildid[$p-1];
		if ($p<count($pildid)-1) $jargmine=$pildid[$p+1];
	} else {
		 header("Location: $myurl");
	}
	include("view/pilt.php");
}

function koik_pildid_id(){
	global $connection;
	$pildid=array();
	$query ="SELECT id FROM 10132492_pildid";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$pildid[]=$row['id'];
	}
	return $pildid;
}

function koik_kommentaarid_id(){
	global $connection;
	$kommentaarid=array();
	$query ="SELECT ID FROM 10132492_kommentaarid";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$kommentaarid[]=$row['ID'];
	}
	return $kommentaarid;
}


function hangi_kommentaarid($id){
	global $connection;
	$kommentaar=array();
	$id=mysqli_real_escape_string($connection, $id);
	if (!is_numeric($id)) return false;
	$query ="SELECT p.ID, p.kommentaar, k.Kasutaja as kasutajaID FROM 10132492_kommentaarid p, 10132492_kasutajad k where p.piltID=$id and k.id=p.kasutajaID ORDER BY p.ID ASC";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$row['kommentaar']=htmlspecialchars($row['kommentaar']);
		$row['kasutaja']=htmlspecialchars($row['kasutajaID']);
		$kommentaar[$row['ID']]=$row;
	}
	return $kommentaar;
}

function komment(){
	global $connection;
	$veateated=array();
	if (isset($_POST["lisakomment"] )){
	if (isset($_POST['kommenttext']) && $_POST['kommenttext']!=""){
		$kommenttext=$_POST['kommenttext'];
	}else {
		$veateated[]="Kommentaar puudu";	
	}	
	if (isset($_POST['id']) && is_numeric($_POST['id'])) {
		$piltID=$_POST['id'];
	}else {
		$veateated[]="Valitud vale pilt";	
    }
	$userID=$_SESSION['user_id']; 
	if(empty($veateated)){
		lisa_kommentaar($userID, $piltID, $kommenttext);
		header("Location: kontroller.php?mode=pilt&id=$piltID");
	} else {
		header("Location: kontroller.php?mode=pilt&id=$piltID");
	}
	}
}

function lisa_kommentaar($userID, $piltID, $kommenttext ) {
	global $connection;

	$userID=mysqli_real_escape_string($connection, $userID);
	$piltID=mysqli_real_escape_string($connection, $piltID);
	$kommenttext=mysqli_real_escape_string($connection, $kommenttext);

	$query ="insert into 10132492_kommentaarid (kasutajaID, piltID, kommentaar) values ('$userID', '$piltID', '$kommenttext')";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
	return mysqli_insert_id($connection);	
}

function kustuta(){
	global $connection;
	$veateated=array();
	if (isset($_POST["kustutakomm"] )){
		if (isset($_POST['id']) && is_numeric($_POST['id'])) {
		$piltID=$_POST['id'];
		}else {
		$veateated[]="Valitud vale pilt";	
		}
		if (isset($_POST['ID']) && $_POST['ID']!=""){
		$ID=$_POST['ID'];
		}else {
		$veateated[]="valitud vale kommentaar";	
		}	
		if(empty($veateated)){
		kustuta_kommentaar($ID);
		header("Location: kontroller.php?mode=pilt&id=$piltID");
		}
	} else {
		header("Location: kontroller.php?mode=pilt&id=$piltID");
	
	}
}

function kustuta_kommentaar($ID) {
	global $connection;

	$ID=mysqli_real_escape_string($connection, $ID);
	$query ="delete from 10132492_kommentaarid where ID='$ID'";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
	return mysqli_insert_id($connection);	
}
?>

